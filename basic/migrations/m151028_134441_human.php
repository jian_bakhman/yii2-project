<?php

use yii\db\Schema;
use yii\db\Migration;

class m151028_134441_human extends Migration
{
    public function up()
    {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('human', [
				'id'		=> Schema::TYPE_PK,
				'name'	=> Schema::TYPE_STRING,
				'age'		=> Schema::TYPE_INTEGER,
				'description'=> Schema::TYPE_STRING,
			], $tableOptions);

		$this->insert('human', [
			'name'	=> 'Vasya',
			'age'		=> '25',
			'description'=> 'same descriptions'
		]);

		$this->insert('human', [
			'name'	=> 'Fedya',
			'age'		=> '22',
			'description'=> 'same descriptions'
		]);

		$this->insert('human', [
			'name'	=> 'Vova',
			'age'		=> '30',
			'description'=> 'same descriptions'
		]);

		$this->insert('human', [
			'name'	=> 'Peter',
			'age'		=> '22',
			'description'=> 'same descriptions'
		]);

		$this->insert('human', [
			'name'	=> 'Jimme',
			'age'		=> '19',
			'description'=> 'same descriptions'
		]);

    }

    public function down()
    {
		$this->dropTable('human');
    }

}
