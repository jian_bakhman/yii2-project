<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Human;


class GalleryController extends Controller
{
  	public function actionGallery()
	{
		return $this->render('gallery');
	}

	public function actionHuman()
	{
		$human = Human::find()->where(['age' =>22])->asArray()->all();

		$addHuman = new Human() ;
		$addHuman->name = 'Ray Johnson';
		$addHuman->age = 35;
		$addHuman->description = 'The new description for new human';
		$saved = $addHuman->save();

		return $this->render('human', [
			'human' => $human,
			'saved'  => $saved,
		]);
	}

}
