<?php

namespace app\models;

use yii\db\ActiveRecord;

class Human extends ActiveRecord
{

	public static function tableName()
	{
		return 'human';
	}

	public function rules() {
		return [
			['name', 'string', 'min' => 3, 'max' => 20,],
			['age', 'integer',],
			['description', 'safe'],
		];

	}

}